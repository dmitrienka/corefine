#!/usr/bin/env python3

import sys
from gemmi import cif
import gemmi

from lammps import lammps
from lammps import PyLammps
import numpy as np
import matplotlib.pyplot as plt
lmp = lammps()
L = PyLammps(ptr= lmp)
lmp.file("ak170.in")


doc = gemmi.read_small_structure("ak170_reax.cif")

coords = [ [ round(y, 8) for y in   x.fract]  for x in doc.get_all_unit_cell_sites()]

elems = [ x.element.name  for x in doc.get_all_unit_cell_sites()]

coords_small = [ [ round(y, 8) for y in   x.fract]  for x in doc.sites]

group = gemmi.find_spacegroup_by_name(doc.spacegroup_hm)

def bound_it(l):
    return [x - floor(x) for x in l]

def expand_naive(small_coords, group):
    expanded = [op.apply_to_xyz(co) for co in small_coords for op in group.operations().sym_ops]
    return [[round(x - floor(x), 8) for x in co] for co in expanded ]

def to_ortho(coords, cell):
     return np.array(coords).dot(cell.orthogonalization_matrix.tolist())


def objLammps(vec):
    coords = to_ortho(expand_naive(numpy.reshape(vec, (-1,3)), group), doc.cell)
    for (atom, pos) in zip(L.atoms, coords):
        atom.position = tuple(pos)
    L.run(0)
    return L.eval("pe")

def gradLammps(vec):
    coords = to_ortho(expand_naive(numpy.reshape(vec, (-1,3)), group), doc.cell)
    for (atom, pos) in zip(L.atoms, coords):
        atom.position = tuple(pos)
    L.run(0)
    forces = np.reshape([L.atoms[i].force  for i in range(0,L.system.natoms)], (-1,8,3))
    return (forces[:,0,:].dot(doc.cell.orthogonalization_matrix.tolist()) * -7.961).flatten()

vec0 = np.reshape(coords_small, (1,-1)).flatten()

from scipy import optimize

my_bounds = [ (x - 0.1 , x + 0.1) for x in vec0]

aa = time.time()
minimized_lbfgb = optimize.minimize(objLammps, vec0, jac = gradLammps,
                                     method = 'L-BFGS-B', bounds = my_bounds, callback = print )
print(time.time() - aa)
