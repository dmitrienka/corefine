#!/usr/bin/env python3

import os,sys
sys.path.insert(0,os.path.expanduser("/home/dmitrienka/miniconda3/envs/corefine/GSASII"))

import GSASIIscriptable as G2sc
import GSASIIstrMath as G2math
import GSASIIstrMain as G2main
from  GSASIIIO import G2stIO

from GSASIIstrMain import G2stMth
from  GSASIIIO import G2mv
import GSASIImpsubs as G2mp
import pytexture as ptx

import copy
import scipy

from memoization import cached

GPXfile = "ak170.gpx"
printFile = open("ak170.log", "w+")


## initialization
## stolen from GSASIIstrMain.Refine()
def gsas_ini(GPXfile, printFile):
    G2mp.InitMP()
    ptx.pyqlmninit()            #initialize fortran arrays for spherical harmonics
    G2stIO.ShowBanner(printFile)
    varyList = []
    parmDict = {}
    G2mv.InitVars()
    Controls = G2stIO.GetControls(GPXfile)
    G2stIO.ShowControls(Controls,printFile)
    calcControls = {}
    calcControls.update(Controls)
    constrDict,fixedList = G2stIO.GetConstraints(GPXfile)
    restraintDict = G2stIO.GetRestraints(GPXfile)
    Histograms,Phases = G2stIO.GetUsedHistogramsAndPhases(GPXfile)
    if not Phases:
        G2fil.G2Print (' *** ERROR - you have no phases to refine! ***')
        G2fil.G2Print (' *** Refine aborted ***')
    if not Histograms:
        G2fil.G2Print (' *** ERROR - you have no data to refine with! ***')
        G2fil.G2Print (' *** Refine aborted ***')
    rigidbodyDict = G2stIO.GetRigidBodies(GPXfile)
    rbIds = rigidbodyDict.get('RBIds',{'Vector':[],'Residue':[]})
    rbVary,rbDict = G2stIO.GetRigidBodyModels(rigidbodyDict,pFile=printFile)
    (Natoms,atomIndx,phaseVary,phaseDict,pawleyLookup,FFtables, BLtables,MFtables,
     maxSSwave) = G2stIO.GetPhaseData(Phases,restraintDict, rbIds,pFile=printFile)
    calcControls['atomIndx'] = atomIndx
    calcControls['Natoms'] = Natoms
    calcControls['FFtables'] = FFtables
    calcControls['BLtables'] = BLtables
    calcControls['MFtables'] = MFtables
    calcControls['maxSSwave'] = maxSSwave
    hapVary,hapDict,controlDict = G2stIO.GetHistogramPhaseData(Phases,Histograms,pFile=printFile)
    TwConstr,TwFixed = G2stIO.makeTwinFrConstr(Phases,Histograms,hapVary)
    constrDict += TwConstr
    fixedList += TwFixed
    calcControls.update(controlDict)
    histVary,histDict,controlDict = G2stIO.GetHistogramData(Histograms,pFile=printFile)
    calcControls.update(controlDict)
    varyList = rbVary+phaseVary+hapVary+histVary
    parmDict.update(rbDict)
    parmDict.update(phaseDict)
    parmDict.update(hapDict)
    parmDict.update(histDict)
    G2stIO.GetFprime(calcControls,Histograms)
    varyListStart = tuple(varyList) # save the original varyList before dependent vars are removed
    msg = G2mv.EvaluateMultipliers(constrDict,parmDict)
    if msg:
        print('Unable to interpret multiplier(s): '+msg)
    G2mv.GenerateConstraints(varyList,constrDict,fixedList,parmDict)
    print(G2mv.VarRemapShow(varyList))
    if 'parmFrozen' not in Controls:
            Controls['parmFrozen'] = {}
    if 'FrozenList' not in Controls['parmFrozen']:
        Controls['parmFrozen']['FrozenList'] = []
    parmFrozenList = Controls['parmFrozen']['FrozenList']
    frozenList = [i for i in varyList if i in parmFrozenList]
    if len(frozenList) != 0:
        varyList = [i for i in varyList if i not in parmFrozenList]
        G2fil.G2Print(
            'Frozen refined variables (due to exceeding limits)\n\t:{}'
            .format(frozenList))
    G2mv.Map2Dict(parmDict,varyList)
    values =  np.array(G2stMth.Dict2Values(parmDict, varyList))
    return (values, [Histograms,Phases,restraintDict,rigidbodyDict], parmDict, varyList, calcControls, pawleyLookup)


ini_things = gsas_ini(GPXfile, printFile)

(values, [Histograms,Phases,restraintDict,rigidbodyDict],
 parmDict, varyList, calcControls, pawleyLookup) = copy.deepcopy(ini_things)


def objFun(vals):
    a2 = G2math.errRefine(vals, [Histograms,Phases,restraintDict,rigidbodyDict],parmDict,varyList,calcControls,pawleyLookup,None)
    return  sum(a2**2)

@cached
def objGradHess(vals):
    return G2main.G2stMth.HessRefine(vals,
                                     [Histograms,Phases,restraintDict,rigidbodyDict],
                                     parmDict,varyList,calcControls,pawleyLookup,None)

def objGrad(vals):
    return objGradHess(vals)[0] * -2.0

def objHess(vals):
    return objGradHess(vals)[1]


my_bounds = [(x - 0.1, x + 0.1) for x in values]
my_bounds[69:83] = [(0.8 * x, 1.2*x) if x >= 0 else (1.2*x, 0.8*x) for x in values[69:83]]



aa = time.time()
minimized_lbfgsb = scipy.optimize.minimize(objFun, values, jac = objGrad,
                                          method = 'L-BFGS-B', bounds = my_bounds, callback = print )
print(time.time() - aa)




t = 0.5


def fullObj(vec, t):
    vec_lammps = vec0 + vec[0:69]
    return t * objFun(vec) + (1 -t)* objLammps(vec_lammps)

def fullGrad(vec):
    vec_lammps = vec0 + vec[0:69]
    grad_gsas = objGrad(vec) * t
    grad_lammps = gradLammps(vec_lammps) * (1 - t)
    grad_gsas[0:69] = grad_gsas[0:69] + grad_lammps
    return grad_gsas


aa = time.time()
minimized_0_5 = scipy.optimize.minimize(fullObj, values, jac = fullGrad,
                                          method = 'L-BFGS-B', bounds = my_bounds, callback = print )
print(time.time() - aa)


t = 0.2

aa = time.time()
minimized_0_2 = scipy.optimize.minimize(fullObj, values, jac = fullGrad,
                                          method = 'L-BFGS-B', bounds = my_bounds, callback = print )
print(time.time() - aa)


new = copy.deepcopy(values)

for t in np.arange(0.0, 1.01, 0.01):
    print('step: ', t)
    aa = time.time()
    minimized = scipy.optimize.minimize(fullObj, new, jac = fullGrad,
                                          method = 'L-BFGS-B', bounds = my_bounds)
    print('time: ', time.time() - aa)
    new = minimized['x']
    my_bounds = [(x - 0.1, x + 0.1) for x in new]
    my_bounds[69:83] = [(0.8 * x, 1.2*x) if x >= 0 else (1.2*x, 0.8*x) for x in new[69:83]]
    print('chi^2: ', objFun(new))
    print('Etot: ', objLammps(new[0:69] + vec0))
    print(new)
    print(np.reshape(new[0:69] + vec0, (-1,3)))


# started [359]
